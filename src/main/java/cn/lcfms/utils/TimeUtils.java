package cn.lcfms.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	
	public enum Formatter{
		SHORT("yyyyMMdd"),_SHORT("yyyy-MM-dd"),LONG("yyyy-MM-dd HH:mm:ss");	
		private String pattern;
		public String getFormatter() {
			return this.pattern;
		}
		Formatter(String pattern) {
			this.pattern=pattern;			
		}		
	}
	
	public static String getCurrentDateTime() {
		String formatter = Formatter._SHORT.getFormatter();
		SimpleDateFormat sdf=new SimpleDateFormat(formatter);
		Date date = new Date();
		String time = sdf.format(date);
		return time;
	}
	
	public static String getCurrentDateTime(Formatter formatter) {	
		SimpleDateFormat sdf=new SimpleDateFormat(formatter.getFormatter());
		Date date = new Date();
		String time = sdf.format(date);
		return time;
	}
	
	public static String getCurrentTime() {
		 long time = new Date().getTime();
		 return String.valueOf(time);
	}
	
	public static String getCurrentShortTime() {
		 long time = new Date().getTime();
		 return String.valueOf(time/1000);
	}
	
	public static String getFormatTime(Formatter formatter,String time) {
		SimpleDateFormat sdf=new SimpleDateFormat(formatter.getFormatter());
		Long l = Long.valueOf(time);
		Date date = new Date(l*1000);
		String r = sdf.format(date);
		return r;
	}
	
	public static void main(String[] args) throws ParseException {
		String formatTime = TimeUtils.getFormatTime(Formatter._SHORT, "1528100636");
		System.out.println(formatTime);
	}
	
}
