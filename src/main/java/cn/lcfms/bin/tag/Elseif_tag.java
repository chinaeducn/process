package cn.lcfms.bin.tag;

import javax.servlet.jsp.tagext.BodyTagSupport;

public class Elseif_tag extends BodyTagSupport{	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean test;
    public boolean isTest() {
		return test;
	}

	public void setTest(boolean test) {
		this.test = test;
	}
	/**
     * @return int
     */
    @Override
    public int doStartTag() {
    	If_tag ifTag = If_tag.getIfTag(this.pageContext);

        if(ifTag == null) {
            throw new RuntimeException("IfTag not match !");
        }

        if(ifTag.complete()) {
            return SKIP_BODY;
        }
        else {
            if(this.test == true) {
                ifTag.finish();
                return EVAL_BODY_INCLUDE;
            }
            else {
            	ifTag.unfinish();
                return SKIP_BODY;
            }
        }
    }
}