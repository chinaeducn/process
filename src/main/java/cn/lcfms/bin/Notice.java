package cn.lcfms.bin;

import net.sf.json.JSONObject;

public class Notice {
	private int code;
	private String msg;
	private String url;
			
	public Notice() {
		
	}
	
	public Notice(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
	public Notice(int code, String msg, String url) {
		this.code = code;
		this.msg = msg;
		this.url = url;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {	
		JSONObject object=new JSONObject();
		object.put("code", code);
		object.put("msg", msg);
		object.put("url", url);
		return object.toString();
	}
	
}
