(function($){
	window.collapse={
        init:function(){
			$('.panel-heading>a[data-toggle=collapse]').click(function(){
				if($(this).hasClass("collapsed")){
					var c=$(this).children("i").attr("class");
					switch(c){
						case "fa fa-chevron-right":
						$(this).children("i").attr("class","fa fa-chevron-down");
						break;
						case "fa fa-plus":
						$(this).children("i").attr("class","fa fa-minus");
						break;
						case "fa fa-hand-o-right":
						$(this).children("i").attr("class","fa fa-hand-o-down");
						break;
						case "fa fa-frown-o":
						$(this).children("i").attr("class","fa fa-smile-o");
						break;
					}
				}else{
					var c=$(this).children("i").attr("class");
					switch(c){
						case "fa fa-chevron-down":
						$(this).children("i").attr("class","fa fa-chevron-right");
						break;
						case "fa fa-minus":
						$(this).children("i").attr("class","fa fa-plus");
						break;
						case "fa fa-hand-o-down":
						$(this).children("i").attr("class","fa fa-hand-o-right");
						break;
						case "fa fa-smile-o":
						$(this).children("i").attr("class","fa fa-frown-o");
						break;
					}
				}			
			});				
		}		
	}	
})($);