<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>老成FMS-BPMN2.0工作流在线编辑器</title>
		<meta name="keywords" content="java快速开发框架,java编程,java web"> 
	    <meta name="description" content="老成fms(Frame Management System)，是一款前端，后台，业务集成的快速开发框架">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
		<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
		<link rel="stylesheet" href="${CSS}ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />	
		<!--[if !IE]> -->
		<script src="${JS}jquery-2.1.4.min.js"></script>
		<!-- <![endif]-->	
		<!--[if IE]>
		<script src="${JS}jquery-1.11.3.min.js"></script>
		<![endif]-->	
		<script src="${JS}../../layer/layer.js"></script>	
		<script src="${JS}../../tag/js/bootstrap-select.js"></script>		
		<link rel="stylesheet" href="${CSS}diagram-js.css">
        <link rel="stylesheet" href="${CSS}bpmn.css">
        <link rel="stylesheet" href="${CSS}app.process.css">
		<link rel="stylesheet" href="${CSS}../../tag/css/bootstrap-select.css">	
		<style>
		.bs-searchbox, .bs-actionsbox, .bs-donebutton{padding:5px 10px;}
		</style>		
</head>
<body class="no-skin">
	<div class="navbar navbar-default" id="navbar">
		<div class="navbar-container" id="navbar-container">
			<div class="navbar-header pull-left">
				<a href="#" class="navbar-brand" style="cursor:default;">
					<small>
						<i class="icon-home"></i>
						老成FMS-BPMN2.0工作流在线编辑器
					</small>
				</a><!-- /.brand -->
			</div><!-- /.navbar-header -->
		</div><!-- /.container -->
	</div>
	<div class="main-container ace-save-state" id="main-container">
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
				<div class="row">
					<div class="col-xs-1"></div>
					<div class="col-xs-11" id="js-drop-zone" style="border:1px dashed grey;"></div>
				</div>
				</div>
				<div class="io-import-export">
				    <ul class="io-import io-control io-control-list io-horizontal">
				       <li>
				        <button data-toggle="tooltip" data-placement="top" data-original-title="导入" id="importProcess">
				          <i class="fa fa-folder"></i>
				        </button>
				      </li>				      
				      <li class="vr" style="margin-left: 2px"></li>
				      <li>
				        <button data-toggle="tooltip" data-placement="top" data-original-title="下载" id="download">
				          <i class="fa fa-download"></i>
				        </button>
				      </li>
				      <li class="vr" style="margin-left: 2px"></li>
				       <li>
				        <button data-toggle="tooltip" data-placement="top" data-original-title="设置" id="setProcess">
				          <i class="fa fa-cog"></i>
				        </button>
				      </li>
				      <li class="vr" style="margin-left: 2px"></li>
				       <li>
				        <button data-toggle="tooltip" data-placement="top" data-original-title="保存" id="saveProcess">
				          <i class="fa fa-save"></i>
				        </button>
				      </li>
				      <li class="vr" style="margin-left: 2px"></li>
				       <li>
				        <button data-toggle="tooltip" data-placement="top" data-original-title="拷贝" id="copyProcess">
				          <i class="fa fa-copy"></i>
				        </button>
				      </li>
				      <li class="vr" style="margin-left: 2px"></li>
				       <li>
				        <button data-toggle="tooltip" data-placement="top" data-original-title="帮助" id="helpProcess">
				          <i class="fa fa-question-circle"></i>
				        </button>
				      </li>
				    </ul>
				  </div>
			</div>
		</div><!-- /.main-content -->
	</div><!-- /.main-container -->	
	<div class="modal fade" id="myModal" style="display: none;">
			<div class="modal-dialog modal-lg">
				<div class="tabbable">
					<ul class="nav nav-tabs" id="myTab">
						<li>
							<a data-toggle="tab" href="#general">
								基本设置
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#dataobject">
								数据对象
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#mainConfig">
								主要参数
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#form">
								定义表单
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#permission">
								权限控制
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#listener">
								设置监听
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#signals">
								发送信号
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#message">
								发送消息
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#multi">
								更多参数
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#bpmnXml">
								bpmn源码
							</a>
						</li>
					</ul>
					<div class="tab-content" style="background:#fff;">
						<div id="general" class="tab-pane"></div>
						<div id="dataobject" class="tab-pane"></div>
						<div id="mainConfig" class="tab-pane"></div>
						<div id="permission" class="tab-pane">
						<form class="form-horizontal" name="permissionData">
							<div class="form-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">
									用户：
								</label>
								<div class="col-sm-10">					
									<select name="admin" class="show-tick form-control selectpicker" data-live-search="true" multiple searchAction="selectUser">
										<c:forEach items="${admin}" var="ad">
										<option value="${ad.aid}" data-subtext="${ad.gdesc} ${ad.oname}">
											${ad.aname}
										</option>	
										</c:forEach> 										
									</select>						
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									角色：
								</label>
								<div class="col-sm-10">					
									<select name="group" class="show-tick form-control selectpicker" data-live-search="true" multiple searchAction="selectGroup">
										<c:forEach items="${group}" var="gd">
										<option value="${gd.gid}">
											${gd.gname}
										</option>
										</c:forEach> 																							
									</select>						
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									机构：
								</label>
								<div class="col-sm-10">					
									<select name="organization" class="show-tick form-control selectpicker" data-live-search="true" multiple searchAction="selectDePart">
										<c:forEach items="${organization}" var="od">
										<option value="${od.oid}">
											${od.oname}
										</option>
										</c:forEach> 			
									</select>						
								</div>
							</div>
							</div>
							<div class="form-footer">
								<div class="form-group">
									<label class="col-sm-2">
									</label>
									<div class="col-sm-10">
										<button type="button" id="permissionSave" class="btn btn-sm btn-grey">
											<i class="fa fa-check">
											</i>
											保存权限
										</button>
									</div>
								</div>
							</div>
						</form>	
						<script>
						$("#permissionSave").click(function(){
							var admin=[];
							var group=[];
							var organization=[];
							$("select[name='admin']>option:selected").each(function(){
								admin.push($(this).val());
							});
							$("select[name='group']>option:selected").each(function(){
								group.push($(this).val());
							});
							$("select[name='organization']>option:selected").each(function(){
								organization.push($(this).val());
							});
							var obj={admin:admin.join(","),group:group.join(","),organization:organization.join(",")};
							shapeElement.value=JSON.stringify(obj);
							synchroniz("savepermission");	
						});
						</script>
						</div>
						<div id="form" class="tab-pane">
						    <div style="overflow:auto;position:relative;">
						    <table class="table table-condensed table-bordered" style="min-width:950px;">
							<tr>
								<th width="10%">Id</th>
								<th width="30%">Name</th>
								<th width="20%">Type</th>
								<th width="5%">Expression</th>
								<th width="5%">Variable</th>
								<th width="5%">Default</th>
								<th width="5%">Readable</th>
								<th width="5%">Writeable</th>
								<th width="5%">Required</th>
								<th width="10%" style="text-align:center;">操作</th>
							</tr>
							<tbody id="formDataTable">
							</tbody>
							</table>
							</div>
							<form class="form-horizontal" name="formData">
							<div class="form-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">
									Id:
								</label>
								<div class="col-sm-10">
									<input name="id" class="form-control" type="text" value="" placeholder="Id">
								</div>								
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									Name:
								</label>
								<div class="col-sm-10">
									<input name="name" class="form-control" type="text" value="" placeholder="Name">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									类型:
								</label>
								<div class="col-sm-3">
									<select class="form-control" name="type" id="formShowType">
									<option value="string">string字符串</option>
									<option value="date">date日期</option>
									<option value="long">long型数字</option>
									<option value="boolean">boolean布尔型</option>									
									<option value="enum">enum枚举</option>									
									</select>
								</div>
								<side class="hide" id="formShowPattern">			
								<div class="col-sm-3">
									<select class="form-control" name="datePattern">
									<option value="yyyyMMdd">yyyyMMdd</option>
									<option value="yyyy-MM-dd">yyyy-MM-dd</option>
									<option value="yyyy-MM-dd HH:mm:ss">yyyy-MM-dd HH:mm:ss</option>								
									<option value="MM/dd/yyyy HH:mm:ss">MM/dd/yyyy HH:mm:ss</option>								
									</select>
								</div>	
								</side>
							</div>
							<script>
							   $("#formShowType").change(function(){
								  var f=$(this).val(); 
								  if(f=='date'){
									  $("#formShowPattern").removeClass("hide")
								  }else{
									  $("#formShowPattern").addClass("hide");
								  }
							   });
							</script>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									参数:
								</label>
								<div class="col-sm-3">
									<input name="expression" class="form-control" type="text" value="" placeholder="Expression">
								</div>
								<div class="col-sm-3">
									<input name="variable" class="form-control" type="text" value="" placeholder="Variable">
								</div>
								<div class="col-sm-3">
									<input name="default" class="form-control" type="text" value="" placeholder="Default">
								</div>
							</div>							
							<div class="form-group">
								<label class="col-sm-2 control-label">
									选项:
								</label>
								<div class="col-sm-3">
									<select class="form-control" name="readable">
									<option value="true">可读</option>
									<option value="true">是</option>
									<option value="false">否</option>								
									</select>
								</div>
								<div class="col-sm-3">
									<select class="form-control" name="writeable">
									<option value="true">可写</option>
									<option value="true">是</option>
									<option value="false">否</option>								
									</select>
								</div>
								<div class="col-sm-3">
									<select class="form-control" name="required">
									<option value="true">必填</option>
									<option value="true">是</option>
									<option value="false">否</option>								
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									form值:
								</label>
								<div class="col-sm-10">
									<table class="table table-condensed table-bordered">
										<tr>
										<th width="45%">Id</th>
										<th width="45%">Name</th>
										<th style="text-align:center;"><a href="#" id="formAddTd"><i class="fa fa-plus fa-lg"></i></a></th>
										</tr>
										<tbody id="formvaluetable">
										</tbody>										
									</table>
								</div>			
							</div>	
							<script>
							$("#formAddTd").click(function(){
								var str="<tr>";
									str+="<td contenteditable=\"true\"></td>";
									str+="<td contenteditable=\"true\"></td>";
									str+="<td style=\"text-align:center;\"><a href=\"#\" class=\"formMinusTd\"><i class=\"fa fa-minus fa-lg\"></i></a></td>";
									str+="</tr>";
								$("#formvaluetable").append(str);
								$(".formMinusTd").click(function(){
									$(this).parent().parent().remove();
								});
							});
							
							</script>							
							</div>
							<div class="form-footer">
								<div class="form-group">
									<label class="col-sm-2">
									</label>
									<div class="col-sm-10">
										<button type="button" id="formSave" class="btn btn-sm btn-grey">
											<i class="fa fa-check">
											</i>
											保存表单
										</button>
									</div>
								</div>
							</div>
							</form>
							<script>
							$("#formSave").click(function(){
								var id=document.formData.id.value;
								if(id==''){
									layer.msg("id不能为空！");
									return false;
								}
								var name=document.formData.name.value;
								var type=document.formData.type.value;
								var datePattern=document.formData.datePattern.value;
								var expression=document.formData.expression.value;
								var variable=document.formData.variable.value;
								var _default=document.formData.default.value;
								var readable=document.formData.readable.value;
								var writeable=document.formData.writeable.value;
								var required=document.formData.required.value;
								var value=[];
								var obj={};	
								var s=$("#formvaluetable>tr>td");
								for(var i=0;i<s.length;i++){
									if(i%3==0)obj.id=$(s[i]).html().trim();
									if(i%3==1)obj.name=$(s[i]).html().trim();
									if(i%3==2){value.push(obj);obj={};}
								}
								var obj={
									id:id,
									name:name,
									type:type,
									datePattern:datePattern,
									expression:expression,
									variable:variable,
									default:_default,
									readable:readable,
									writeable:writeable,
									required:required,
									value:value
								};
								var b=true;
								for(var i=0;i<shapeElement.form.length;i++){	
									if(shapeElement.form[i].id==id){
										shapeElement.form[i]=obj;
										b=false;
										break;
									}
								}
								if(b){
									shapeElement.form.push(obj);
								}
								shapeElement.value=JSON.stringify(shapeElement.form);
								synchroniz("saveForm",function(){
									showFormData();
									document.formData.reset();
									$("#formvaluetable").html("");
								});
							});
							</script>
						</div>
						<div id="listener" class="tab-pane"></div>
						<div id="signals" class="tab-pane"></div>
						<div id="message" class="tab-pane"></div>
						<div id="multi" class="tab-pane"></div>
						<div id="bpmnXml" class="tab-pane"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="io-dialog keybindings-dialog" id="helpDialog">
			<div class="content bindings-default">
			  <h1>快捷键</h1>
			  <table>
				<tbody>
				  <tr>
					<td>撤销</td>
					<td class="binding"><code>ctrl + Z</code></td>
				  </tr>
				  <tr>
					<td>重做</td>
					<td class="binding"><code>ctrl + ↑ + Z</code></td>
				  </tr>
				  <tr>
					<td>全选</td>
					<td class="binding"><code>ctrl + A</code></td>
				  </tr>
				  <tr>
					<td>上下滚动</td>
					<td class="binding"><code>ctrl + Scrolling</code></td>
				  </tr>
				  <tr>
					<td>左右滚动</td>
					<td class="binding"><code>ctrl + ↑ + Scrolling</code></td>
				  </tr>
				  <tr>
					<td>编辑</td>
					<td class="binding"><code>E</code></td>
				  </tr>
				  <tr>
					<td>手型工具</td>
					<td class="binding"><code>H</code></td>
				  </tr>
				  <tr>
					<td>选择工具</td>
					<td class="binding"><code>L</code></td>
				  </tr>
				  <tr>
					<td>坐标工具</td>
					<td class="binding"><code>S</code></td>
				  </tr>
				  <tr>
					<td></td>
					<td class="binding"><code>完整版请联系作者，QQ群：348455534</code></td>
				  </tr>
				</tbody>
			  </table>
			  <a style="position:absolute;top:4px;right:10px;" href="javascript:$('#helpDialog').removeClass('open');"><i class="fa fa-close fa-2x"></i></a>
			</div>			
		</div>
		<div id="formbox" style="display:none;">
		    <side type="text">
				<div class="form-group">
					<label class="col-sm-2 control-label">
						{title}：
					</label>
					<div class="col-sm-10">
						<input onblur="changeValue(this);" class="form-control" act="{act}" type="text" value="{value}">
					</div>
				</div>
			</side>	
			<side type="readonly">
				<div class="form-group">
					<label class="col-sm-2 control-label">
						{title}：
					</label>
					<div class="col-sm-10">
						<input readonly class="form-control" type="text" value="{value}">
					</div>
				</div>
			</side>	
			<side type="textarea">
				<div class="form-group">
					<label class="col-sm-2 control-label">
						{title}：
					</label>
					<div class="col-sm-10">
						<textarea onblur="changeValue(this);"  act="{act}" class="form-control" rows="5" cols="50">{value}</textarea>
					</div>
				</div>
			</side>	
			<side type="select">
				<div class="form-group">
					<label class="col-sm-2 control-label">
						{title}：
					</label>
					<div class="col-sm-10">
						<select class="form-control" act="{act}" onchange="changeValue(this);">{option}</select>
					</div>
				</div>
			</side>	
			<side type="checkonebox">
				<div class="form-group">
					<label class="col-sm-2 control-label">
						{title}：
					</label>
					<div class="col-sm-10">					
						<label style="position:relative;top:7px;">
						<input type="checkbox" value="true" act="{act}" onchange="changeCheck(this);" {checked}/>	
						</label>						
					</div>
				</div>
			</side>				
		</div>							
</body>
<script src="${JS}bootstrap.min.js"></script>
<script src="${JS}showtag.js"></script>
<script src="${JS}bpmn-modeler.development.js"></script>
<script>
$('[data-toggle=tooltip]').tooltip();
$("#js-drop-zone").height(screen.height-240);
var bpmnModeler = new BpmnJS({
   container: '#js-drop-zone',
   keyboard: {
      bindTo: window
   }
});

var shapeElement={};
String.prototype.startsWith = function(str) {	
	if(this.indexOf(str)==0){
		return true;
	}else{
		return false;
	}
}

function synchroniz(act,callback){
	var index = layer.load(1, {shade: [0.1,'#fff']});
	$.post("${APP}synchroniz",{
		xml:exportDiagram(),
		type:shapeElement.type,
		id:shapeElement.id,
		value:shapeElement.value,
		act:act
	},function(res){        	
		 bpmnModeler.importXML(res.xml); 
		 shapeElement=res;
	     if(callback){
	        callback();
	     }
		 layer.close(index);
	});
} 


//初始化加载bpmn文件
var version="${version}";
var deploymentId="${deploymentId}";
if(version && deploymentId){
	shapeElement.value=JSON.stringify({version:version,deploymentId:deploymentId});
	synchroniz("edit");
}else{
	synchroniz("start");
}

function showGeneralBefore(element){
	shapeElement.id=element.id;
	synchroniz("show",function(){
		$('#myModal').modal('toggle');	
		showTab();
		$('#myTab a[href="#general"]').tab('show');	
	});	
}

function showTab(){
	shapeElement.tab=['general','mainConfig','permission','form','listener','multi'];
	//['general','dataobject','mainConfig','permission','form','listener','signals','message','multi','bpmnXml']
	$("#myTab>li").hide();
	switch(shapeElement.type){
		case "process":
		shapeElement.tab=['general','dataobject','listener','signals','message','bpmnXml'];
		break;
		case "sequenceFlow":
		shapeElement.tab=['general','mainConfig','listener'];
		break;
		case "parallelGateway": 
		case "exclusiveGateway":
		case "inclusiveGateway": 
		case "eventBasedGateway":
		shapeElement.tab=['general','listener'];
		break;
		case "manualTask":
		shapeElement.tab=['general','listener','multi'];
		case "endEvent":
		shapeElement.tab=['general','listener'];
		case "intermediateThrowEvent":
		case "intermediateCatchEvent":
		shapeElement.tab=['general','mainConfig','listener'];
		break;
	}
	for(var i=0;i<shapeElement.tab.length;i++){
		$("#myTab a[href='#"+shapeElement.tab[i]+"']").parent().show();
	}
}

function changeValue(ele){
	var value=$(ele).val();
	var act=$(ele).attr("act");
	shapeElement.value=value;
	if(act=='changeStartEvent' || act=='changeTask' || act=='changeGateway' || act=='changeEndEvent' || act=='changeIntermediate'){
		synchroniz(act,changeModal);	
	}else{
		synchroniz(act);	
	}	
}

function changeCheck(ele){
	var value=ele.checked;
	var act=$(ele).attr("act");
	if(value){
		shapeElement.value="true";
	}else{
		shapeElement.value="false";
	}	
	synchroniz(act);	
}

function changeModal(){
	 var general="";
	 var mainConfig="";
	 general+=getEdit("readonly","Id",shapeElement.id);
	 general+=getEdit("text","Name",shapeElement.name,"changeAttribute(name)");
	 //流程本身
	 if(shapeElement.type=="process"){
		 general+=getEdit("text","命名空间",shapeElement.namespace,"changeNamespace");
		 general+=getEdit("textarea","文档",shapeElement.documentation,"changeDocument");
	 }
	 //开始事件
	 if(shapeElement.type=="startEvent"){
		 var data=[
			 {text:'StartEvent',value:''},
			 {text:'TimerStartEvent',value:'timerEventDefinition'},
			 {text:'MessageStartEvent',value:'messageEventDefinition'},
			 {text:'ErrorStartEvent',value:'errorEventDefinition'},
			 {text:'SignalStartEvent',value:'signalEventDefinition'}
			 ];		
		 for(var i=0;i<data.length;i++){
			 if(shapeElement.startType==data[i].value){
				  data[i].selected=true;
			 } 
		 }	 
		 general+=getEdit("select","事件类型",data,"changeStartEvent");	
		 general+=getEdit("textarea","文档",shapeElement.documentation,"changeDocument");
		 general+=getEdit("checkonebox","异步",shapeElement.async,"changeAttribute(activiti:async)");	 
		 general+=getEdit("checkonebox","专有",shapeElement.exclusive,"changeAttribute(activiti:exclusive)");
         if(shapeElement.startType==''){
			 mainConfig+=getEdit("text","initiator",shapeElement.initiator,"changeAttribute(activiti:initiator)");
			 mainConfig+=getEdit("text","formKey",shapeElement.formKey,"changeAttribute(activiti:formKey)");
		 }	 
	 }
	 //结束事件
	 if(shapeElement.type=="endEvent"){
		 var data=[
			 {text:'EndEvent',value:''},
			 {text:'ErrorEndEvent',value:'errorEventDefinition'},
			 {text:'TerminateEndEvent',value:'terminateEventDefinition'},
			 {text:'CancelEndEvent',value:'cancelEventDefinition'}		
			 ];		
		 for(var i=0;i<data.length;i++){
			 if(shapeElement.endType==data[i].value){
				  data[i].selected=true;
			 } 
		 }	 
		 general+=getEdit("select","结束类型",data,"changeEndEvent");	
		 general+=getEdit("checkonebox","异步",shapeElement.async,"changeAttribute(activiti:async)");	 
		 general+=getEdit("checkonebox","专有",shapeElement.exclusive,"changeAttribute(activiti:exclusive)");
		 if(shapeElement.endType=='errorEventDefinition'){
			  mainConfig+=getEdit("text","错误编码",shapeElement.errorRef,"changeChildAttribute(errorEventDefinition,errorRef)");
		 }
	 }
	 //流
	 if(shapeElement.type=="sequenceFlow"){		
		 general+=getEdit("textarea","文档",shapeElement.documentation,"changeDocument");		
		 mainConfig+=getEdit("text","跳过表达式",shapeElement.skipExpression,"changeAttribute(skipExpression)");	
		 mainConfig+=getEdit("textarea","条件表达式",shapeElement.conditionExpression,"changeConditionExpression");		 
	 }
	 //中间事件
	 if(shapeElement.type=="intermediateCatchEvent" || shapeElement.type=="intermediateThrowEvent"){
		var data=[
		     {text:'NoneThrowEvent',value:'NoneThrowEvent'},
			 {text:'SignalCatchEvent',value:'SignalCatchEvent'},
			 {text:'SignalThrowEvent',value:'SignalThrowEvent'},
			 {text:'TimerCatchEvent',value:'TimerCatchEvent'},
			 {text:'MessageCatchEvent',value:'MessageCatchEvent'},				 		
			 {text:'CompensationThrowingEvent',value:'CompensationThrowingEvent'}	
			 ];		
		 for(var i=0;i<data.length;i++){
			 if(shapeElement.intermediateType==data[i].value){
				  data[i].selected=true;
			 } 
		 }	 
		 general+=getEdit("select","事件类型",data,"changeIntermediate");			 
		 general+=getEdit("textarea","文档",shapeElement.documentation,"changeDocument");			 	 
	 }
	 //网关
	 if(
	 shapeElement.type=="parallelGateway" || 
	 shapeElement.type=="exclusiveGateway" || 
	 shapeElement.type=="inclusiveGateway" || 
	 shapeElement.type=="eventBasedGateway"
	 ){
		 var data=[
			 {text:'parallelGateway',value:'parallelGateway'},
			 {text:'exclusiveGateway',value:'exclusiveGateway'},
			 {text:'inclusiveGateway',value:'inclusiveGateway'},
			 {text:'eventBasedGateway',value:'eventBasedGateway'}		
			 ];		
		 for(var i=0;i<data.length;i++){
			 if(shapeElement.type==data[i].value){
				  data[i].selected=true;
			 } 
		 }	 
		 general+=getEdit("select","网关类型",data,"changeGateway");	
		 general+=getEdit("textarea","文档",shapeElement.documentation,"changeDocument");
		 general+=getEdit("checkonebox","异步",shapeElement.async,"changeAttribute(activiti:async)");	 
		 general+=getEdit("checkonebox","专有",shapeElement.exclusive,"changeAttribute(activiti:exclusive)");	
	 }
	 //任务
	 if
	 (
	 shapeElement.type=="userTask" || 
	 shapeElement.type=="scriptTask" || 
	 shapeElement.type=="serviceTask" || 
	 shapeElement.type=="mailTask" || 
	 shapeElement.type=="manualTask" || 
	 shapeElement.type=="receiveTask" || 
	 shapeElement.type=="businessRuleTask" || 
	 shapeElement.type=="callActivity"
	 ){
		 var data=[
			 {text:'userTask',value:'userTask'},
			 {text:'manualTask',value:'manualTask'},
			 {text:'scriptTask',value:'scriptTask'},
			 {text:'serviceTask',value:'serviceTask'},
			 {text:'mailTask',value:'mailTask'},
			 {text:'receiveTask',value:'receiveTask'},
			 {text:'businessRuleTask',value:'businessRuleTask'},
			 {text:'callActivity',value:'callActivity'}			
			 ];		
		 for(var i=0;i<data.length;i++){
			 if(shapeElement.type==data[i].value){
				  data[i].selected=true;
			 } 
		 }	 
		 general+=getEdit("select","任务类型",data,"changeTask");	
		 general+=getEdit("textarea","文档",shapeElement.documentation,"changeDocument");
		 general+=getEdit("checkonebox","异步",shapeElement.async,"changeAttribute(activiti:async)");	 
		 general+=getEdit("checkonebox","专有",shapeElement.exclusive,"changeAttribute(activiti:exclusive)");	 
		 general+=getEdit("checkonebox","补偿",shapeElement.isForCompensation,"changeAttribute(isForCompensation)");
		 if(shapeElement.type=='userTask'){
			 mainConfig+=getEdit("text","Assignee",shapeElement.assignee,"changeAttribute(activiti:assignee)");
			 mainConfig+=getEdit("text","formKey",shapeElement.formKey,"changeAttribute(activiti:formKey)");
			 mainConfig+=getEdit("text","Due Date",shapeElement.dueDate,"changeAttribute(activiti:dueDate)");
			 mainConfig+=getEdit("text","Category",shapeElement.category,"changeAttribute(activiti:category)");
			 mainConfig+=getEdit("text","Priority",shapeElement.priority,"changeAttribute(activiti:priority)");
			 mainConfig+=getEdit("text","Skip Expression",shapeElement.skipExpression,"changeAttribute(activiti:skipExpression)");
		 }	 
	 }
	 $("#general").html("<form class='form-horizontal'><div class='form-body'>"+general+"</div></form>");
	 $("#mainConfig").html("<form class='form-horizontal'><div class='form-body'>"+mainConfig+"</div></form>");
	 //处理表单
	 showFormData();
}

$('#myModal').on('show.bs.modal', function (e) {
	changeModal();
});

$("#myTab a[href='#bpmnXml']").on("shown.bs.tab", function(e){
	var code=exportDiagram();
	$("#bpmnXml").html("<code>"+html_encode(code)+"</code>");
});

//点击设置流程
$("#setProcess").click(function(){
	synchroniz("process",function(){
		$('#myModal').modal('toggle');
		showTab();
		$('#myTab a[href="#general"]').tab('show');	
	});	
});

$("#saveProcess").click(function(){
	$.post("${APP}save",exportDiagram(),function(res){	
		layer.msg(res); 
	},'text');
});

$("#helpProcess").click(function(){
	$("#helpDialog").addClass("open");
});

$("#download").click(function(){
	doSave(exportDiagram(),"application/xml;charset=UTF-8","process.bpmn");
});

function doSave(value, type, name) {  
    var blob;  
    if (typeof window.Blob == "function") {  
        blob = new Blob([value], {type: type});  
    } else {  
        var BlobBuilder = window.BlobBuilder || window.MozBlobBuilder || window.WebKitBlobBuilder || window.MSBlobBuilder;  
        var bb = new BlobBuilder();  
        bb.append(value);  
        blob = bb.getBlob(type);  
    }  
    var URL = window.URL || window.webkitURL;  
    var bloburl = URL.createObjectURL(blob);  
    var anchor = document.createElement("a");  
    if ('download' in anchor) {  
        anchor.style.visibility = "hidden";  
        anchor.href = bloburl;  
        anchor.download = name;  
        document.body.appendChild(anchor);  
        var evt = document.createEvent("MouseEvents");  
        evt.initEvent("click", true, true);  
        anchor.dispatchEvent(evt);  
        document.body.removeChild(anchor);  
    } else if (navigator.msSaveBlob) {  
        navigator.msSaveBlob(blob, name);  
    } else {  
        location.href = bloburl;  
    }  
}

function showFormData(){
	if(!shapeElement.form){
		$("#formDataTable").html("");
		return;
	}
	var str="";
	var form=shapeElement.form;
	for(var i=0;i<form.length;i++){		
		var type=form[i].type;
		var datePattern=form[i].datePattern;
		if(type=='date'){
			type+="("+datePattern+")";
		}
		str+="<tr><td>"+form[i].id+"</td><td>"+form[i].name+"</td><td>"+type+"</td><td>"+form[i].expression+"</td><td>"+form[i].variable+"</td><td>"+form[i].default+"</td><td>"+form[i].readable+"</td><td>"+form[i].writeable+"</td><td>"+form[i].required+"</td><td style=\"text-align:center;\"><a href=\"#\" tid=\""+form[i].id+"\" class=\"formEditTd\"><i class=\"fa fa-edit fa-lg\"></i></a>&nbsp;&nbsp;<a href=\"#\" tid=\""+form[i].id+"\" class=\"formMinusTd\"><i class=\"fa fa-minus fa-lg\"></i></a></td></tr>"		
	}
	$("#formDataTable").html(str);
	$(".formMinusTd").click(function(){
		var id=$(this).attr("tid");
		var form=shapeElement.form;
		for(var i=0;i<form.length;i++){	
			if(form[i].id==id){
				shapeElement.form.splice(i, 1);
			}
		}
		shapeElement.value=JSON.stringify(shapeElement.form);
		synchroniz("saveForm",function(){
			showFormData();
		});	
	});
	$(".formEditTd").click(function(){
		$("#formvaluetable").html("");
		var id=$(this).attr("tid");
		var form=shapeElement.form;
		for(var i=0;i<form.length;i++){	
			if(form[i].id==id){
				document.formData.id.value=form[i].id;
				document.formData.name.value=form[i].name;
				$(document.formData.type)[0].value=form[i].type;
				document.formData.datePattern.value=form[i].datePattern;
				document.formData.expression.value=form[i].expression;
				document.formData.variable.value=form[i].variable;
				document.formData.default.value=form[i].default;
				$(document.formData.readable)[0].value=form[i].readable;
				$(document.formData.writeable)[0].value=form[i].writeable;
				$(document.formData.required)[0].value=form[i].required;
				if(!form[i].value)return;
				for(var j=0;j<form[i].value.length;j++){
					var str="<tr>";
					str+="<td contenteditable=\"true\">"+form[i].value[j].id+"</td>";
					str+="<td contenteditable=\"true\">"+form[i].value[j].name+"</td>";
					str+="<td style=\"text-align:center;\"><a href=\"#\" class=\"formMinusTd\"><i class=\"fa fa-minus fa-lg\"></i></a></td>";
					str+="</tr>";
					$("#formvaluetable").append(str);
				}		
				$(".formMinusTd").click(function(){
					$(this).parent().parent().remove();
				});
			}
		}
	});
}

function getEdit(type,title,value,act){
	var formbox=$("#formbox>side[type="+type+"]").html();
	formbox=formbox.replace(/{title}/g,title);
	formbox=formbox.replace(/{act}/g,act);
	if(type=='select'){
		var option="";
		for(var i=0;i<value.length;i++){
			if(value[i].selected){
				option+="<option selected value='"+value[i].value+"'>"+value[i].text+"</option>";
			}else{
				option+="<option value='"+value[i].value+"'>"+value[i].text+"</option>";
			}
		}
		formbox=formbox.replace(/{option}/g,option);
	}
	if(type=='checkonebox'){
		if(value=='true'){
			formbox=formbox.replace(/{checked}/g,"checked");
		}else{
			formbox=formbox.replace(/{checked}/g,"");
		}	
	}
	if(value==undefined){
		formbox=formbox.replace(/{value}/g,"");
	}else{
		formbox=formbox.replace(/{value}/g,value);
	}	
	return formbox;
}

function exportDiagram() {
	var r="";
    bpmnModeler.saveXML({ format: true }, function(err, xml) {
    if (err) {
        return r;
    }
    r=xml;
    });
    return r;
}


</script>
</html>
